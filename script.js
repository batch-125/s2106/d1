

//Converting the ERD into JSON-like syntax

// user.json
{
	"_id": "507f1f77bcf86cd799439011",
	"firstName": "John",
	"lastName": "Foreign",
	"email": "jf@gmail.com",
	"password": "jf12345",
	"isAdmin": false,
	"mobileNum": "12345678910",
	"datetimeRegistered": "2021-08-24T14:49:00.00Z"
}

//courses.json
{
	"_id": "507f191e810c19729de860ea",
	"name": "fullt stack web development",
	"description": "learn how to be a web dev",
	"price": 3000,
	"isActive": true,
	"dateTimeCreated": "2021-08-24T14:49:00.00Z"
}

//user_enrollment.json
{
	"_id": "618f192e820c19759de840ea",
	"userId": "507f1f77bcf86cd799439011",
	"courseId": "507f191e810c19729de860ea",
	"dateTimeEnrolled": "2021-08-24T14:49:00.00Z"
}


//Converting the ERD into JSON-like syntax

// user.json
{
	"_id": "507f1f77bcf86cd799439011",
	"firstName": "John",
	"lastName": "Foreign",
	"email": "jf@gmail.com",
	"password": "jf12345",
	"isAdmin": false,
	"mobileNum": "12345678910",
	"datetimeRegistered": "2021-08-24T14:49:00.00Z"
}

//courses.json
{
	"_id": "507f191e810c19729de860ea",
	"name": "fullt stack web development",
	"description": "learn how to be a web dev",
	"price": 3000,
	"isActive": true,
	"dateTimeCreated": "2021-08-24T14:49:00.00Z"
}

//user_enrollment.json
{
	"_id": "618f192e820c19759de840ea",
	"userId": "507f1f77bcf86cd799439011",
	"courseId": "507f191e810c19729de860ea",
	"dateTimeEnrolled": "2021-08-24T14:49:00.00Z"
}

db - database
users - collection 
insertOne() - create method
db.users.insertOne(

	{
	"firstName": "John",
	"lastName": "Smith"
	}
);

db.users.insertOne({"firstName": "John","lastName": "Smith"});
// check it is added in users collection

// Read method
	// db - database
	// user - collection
	// find(); - Read Method

db.user.find();